Contributors

- Prachi Singhania (prachisinghania9@gmail.com)  
- Bradley Barnes (bradley.barnes21@gmail.com)  
  
  
# Reflect  
>  Take time in your day to reflect on your goals  
>  your habits  
>  and everything that you should be doing.  
